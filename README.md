# CLASS

# WORK IN PROGRESS - Documentation - 0.0.1

*frenglish totally understandable documentation*

```yaml
<project>: 
    version: 0.0.1
```

# Deroulement
- 1 Month Workshop
- Boostrap Class
- Learn by doing
- Avoid error typing
- Manage thinking
- Upgrade skills by learning fast and finding solutions by yourself
- you'll finish with 2vm
- one server for kubernetes
- one workvm with i3 in arch 

=> ${SUCCES_GARANTY}

# WORKSHOP- [Linux, Docker, Kubernetes]

Installer un serveur Ubuntu, qui fera tourner un node kubernetes avec X applications :

Pour cela nous avons besoin d'un docker-compose qui genere ces X services accompagne d'un fichier .env

Le tout devra etre associe a un repo git 

Toutes les technos disponibles :

- GITLAB        devOps
- NGINX         proxy                       serve production ready build of vuejs webapp, also create https with free letsencrypt https certificates             
- LETSENCRYPT   https *.pem                 4 files from letsencrypt
- WEBAPP        vuejs                       framework writing html with pug lang and css with stylus using grid features
- CMS           strapi-staging              https://strapi.io/ 
- CMS           strapi-production           https://strapi.io/ 
- DB            postgresql                  master
- DB            postgresql_backup           slave
- DB            redis                       find db driver for needed lang
- DB            couchdb                     find db driver for needed lang
- DB            mongodb                     find db driver for needed lang
- API           python                      https://aiohttp.readthedocs.io/en/stable/   async REST YAML api
- API           go                          https://echo.labstack.com/                  cryptocurrency values ticker        
- API           rust                        https://rocket.rs/                          REST api

Ensuite nous automatiseront la sauvegarde et la restauration des données des containers :
- DB        pout la data presente en base
- CMS       permettant de retrouver un état X du système

Afin de garantir la Restauration 
*auto-config des différents services correspond a une sauvegarde timestamp defini par un **UUID** : unique identyfier*

# Documenter et Automatiser chaque etapes en fournissant des logs et tests dans un script python
- from subprocess import run
- steps = { 0 : run("echo y0 from python", shell=True) }
- for key in steps.keys():
    print(f"steps[ {key} ] =returns> {steps[key].values.stdout")

Monitor and Securize the server

# créer deux machines virtuelle

## Installer Ubuntu Server 

- VirtualBox-VM
    - 25% Ram
    - 2 processors
    - 2 hard drives sata 
        - dynamic 10GB
    - network bridge 

- DL ISO http://archive.ubuntu.com/ubuntu/dists/bionic/main/installer-amd64/current/images/netboot/mini.iso

- RUN
    - Command line install
    - Entire disk
    - Grub sda
    - Eject iso after succes while rebooting

<!-- # Installer Manjaro i3 Arch Linux
- 60% Ram
- max processors
- 1 hard drive sata
  - dynamic 10GB -->

<!-- # START WITH UBUNTU THEN ARCH -->

# Connect with ssh
- apt install openssh-server
- > ssh user@ip

<!-- # git
- install git
- create repository for soutenance -->

# zsh / oh my zsh
- apt-get update && apt-get upgrade
- apt-get install zsh

- install oh my zsh 
    - > sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
- chsh -s $(which zsh) # define zsh by default

- change default theme in zshrc with one of thoses : https://github.com/ohmyzsh/ohmyzsh/wiki/Themes

- create alias in zshrc
    - alias apti="apt-get install"
    - alias aptr="apt-get remove"
    - alias apts="apt-get search"
    
- create functions in zshrc

    - git add all
    ```sh=zsh
    git_add_all(){
        echo "git add all commit and push to branch"
        git add --all
        git commit -am
        git push origin ${1}
    }
    ```

<!-- # monitor io input on file and auto push on git
- .zshrc -->

# Créer partition backup with fdisk
- mkdir ~/backup # creation du dossier pour monter la partition du disque 2
- fdisk -l #lister les disques
- fdisk /dev/sdb utiliser fdisk sur le disque /dev/sdb
    - n # créer une nouvelle partition
    - p # de type primaire
        - repondre default a toutes les options
    - w # ecrire les modifications sur la table de partitions
- mkfs -t ext4 /dev/sdb1 # formatter la partition
- édit fstab pour monter le systeme de fichier au demarrage
    - cat /etc/fstab
    - sudo blkid # pour recuperer l'UUID
    - creer une entree dans fstab pour monter la partition backup avec les options ext4 by defaults

# backup folder with rsync and zip

- > mkdir tests
- > mkdir scripts

- create script backup_folder.sh in ~/scripts

```sh=zsh
#!/bin/sh
# rsync PATH_SOURCE PATH_DESTINATION
rsync ~/tests/* ~/backup
```

- populate ~/tests
    - touch ~/tests/file-001

- rendre le script executable
    - chmod +x backup_folder.sh

- executer le script 
    - ./backup_folder.sh

# Automatize backup with system d
- apt get install systemd
- create service file backup_folder.service

```
[Unit]      
Description=backup_folder script 

[Service]                   
ExecStart=/home/$USER/scripts/backup_folder.sh                                                                                                                
[Install]                                                                                                                      
WantedBy=multi-user.target         
```

- copy service file in /etc/systemd/system
- systemctl enable backup_folder

# Install Docker Docker-Compose
- apt install docker docker-compose
- sudo systemctl enable docker
- sudo systemctl start docker
- sudo docker run hello-world

# Reproduire installation Ubuntu avec un Dockerfile

- mkdir ~/docker
- mkdir ~/docker/001
- cd ~/docker/001
- nano Dockerfile

```Dockerfile
FROM ubuntu:latest
RUN apt update -y
RUN apt upgrade -y
RUN apt install git zsh curl iputils-ping -y
RUN sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
CMD ["ping", "8.8.8.8"]%
```

- nano docker-compose.yml
```
version: "3.3"

services:

  ubuntu-001:
    build:
      context: .
      dockerfile: Dockerfile
    restart: always
    hostname: ubuntu-001
    container_name: ubuntu-001
    # partage du volue container vub001 : /vub001
    volumes:
      - vub001:/vub001
      
volumes:
  vub001:
```

- sudo docker-compose up # lance l'infra
- sudo docker exec -it ubuntu-001 zsh # se connecte au container ubuntu-001
- sudo docker inspect ubuntu-001 # affiches les informations relatives au container
- on remarque que le dossier /vub001 sur le serveur correspond au dossier /var/lib/docker/volumes/ub_vub001/_data
```
"Mounts": [
    {
        "Type": "volume",
        "Name": "ub_vub001",
        "Source": "/var/lib/docker/volumes/ub_vub001/_data",
        "Destination": "/vub001",
        "Driver": "local",
        "Mode": "rw",
        "RW": true,
        "Propagation": ""
    }
],
```

# Extract estiam-101-docker-compose.zip
- aller dans le dossier _Asterope_

## Ajout d'un fichier d'environnement
- creer un fichier .env
- inserer les variables d'environnements dans le fichier .env
- example des fichier d'env du docker compose a remplacer
```
# location of postgreSQL database files
PGDATA=/var/lib/postgresql/orion
# name of the default database
POSTGRES_DB=Estiam
# name of the default user
POSTGRES_USER=estiam
# definir le mot de passe de la base de donnees
POSTGRES_PASSWORD=asdiuhgwqiydgasitdwq
```

## Modification du Dockerfile strapi avec les nouvelles informations de database
- modify this line
> RUN strapi new  --dbclient postgres --dbhost postgres --dbport 5432  --dbname Asterope  --dbusername asterope --dbpassword 3d48ce50fbcd439c8c0416190d302bd7 Asterope

## launch docker-compose with
- get ip of asterope-strapi container
- acces admin panel of strapi
- http://<ip>:1337/admin

## backup and restore postgres database content
- editer les scripts contenu dans ce fichier pour sauvegarder le contenu des container postgres et strapi
    - https://gitlab.com/_seshat_/estiam-linux-docker-kubernetes/tree/master/bakrst

# Docker Theory
- build
- daemons
- volumes
- network
- scp
- it

--- 

# DRAFTING

# Manage Docker
- image
- volumes
- network
- deployment
- interactive shell 

# extract estiam-101-docker-compose
- replace all occurences with this technic : https://www.cyberciti.biz/faq/how-to-use-sed-to-find-and-replace-text-in-files-in-linux-unix-shell/
- run multiples docker-compose and analyze host ressources using htop 
- freemem and clean dead processus

# Manage Kubernetes *using minikube*
- pods
- nodes
- load-balancer
- master-slave

# Create Pipelines
- Creer une pipeline gitlab-ci/cd
